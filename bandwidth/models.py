from django.db import models
from django.contrib.auth.models import User
import silk

# Create your models here.
class Network(models.Model):

    network_name = models.CharField(max_length=255, unique=True)
    network_addr = models.CharField(max_length=20)

    def __unicode__(self):
        return u'%s'%(self.network_name)

    @classmethod
    def getIPWildcard(cls, net_name):
        net = Network.objects.get(
            network_name = "%s"%(net_name)
            )

        if net:
            return silk.IPWildcard(str(net.network_addr))
        else:
            raise ("Network not found: %s"%(net))

class NetworkGroup(models.Model):

    group_name = models.CharField(max_length=255)
    networks = models.ManyToManyField(Network)

    def __unicode__(self):
        return u'%s'%(self.group_name)

    @classmethod
    def getIPSets(cls, grp_name):
        grp = NetworkGroup.objects.get(
            group_name = "%s"%(grp_name)
            )

        ipset = silk.IPSet()
        for net in grp.networks.all():
            ipset.update(Network.getIPWildcard(net.network_name))

        if ipset.cardinality() < 1:
            return None
        else:
            return ipset

class NetworkSensor(models.Model):

    SENSOR_CHOICES = [("all", "all")]
    for s in silk.sensors():
        SENSOR_CHOICES.append((s, s))

    sensor_name = models.CharField(max_length=255,
            choices=SENSOR_CHOICES,
            default="all",
            primary_key=True)

    sensor_discription = models.CharField(max_length=255)
    max_bandwidth = models.IntegerField()

    def __unicode__(self):
        return u'%s'%(self.sensor_name)


class NetworkDefault(models.Model):
    """
    We will use the group to determine which networks 
    are internal and use that info like:
     --saddress=blah or --daddress=blah
    """
    default_group = models.ForeignKey(NetworkGroup)
    default_sensor = models.ForeignKey(NetworkSensor)
    user = models.ForeignKey(User, primary_key=True)

    def __unicode__(self):
        return u'%s'%(self.default_group)

    @classmethod
    def getDefaultIPSet(cls, user_name):
        defgrp = NetworkDefault.objects.get(
            user__username = "%s"%(user_name)
            )
        return NetworkGroup.getIPSets(defgrp.default_group.group_name)

