from django.contrib import admin
from bandwidth.models import Network, NetworkGroup, NetworkDefault, NetworkSensor


class NetworkAdmin(admin.ModelAdmin):
    list_display = ('network_name', 'network_addr')


class NetworkGroupAdmin(admin.ModelAdmin):
    list_display = ('group_name',)


class NetworkDefaultAdmin(admin.ModelAdmin):
    list_display = ('default_group', 'default_sensor', 'user')


class NetworkSensorAdmin(admin.ModelAdmin):
    list_display = ('sensor_name', 'max_bandwidth', 'sensor_discription')

admin.site.register(Network, NetworkAdmin)
admin.site.register(NetworkGroup, NetworkGroupAdmin)
admin.site.register(NetworkDefault, NetworkDefaultAdmin)
admin.site.register(NetworkSensor, NetworkSensorAdmin)
