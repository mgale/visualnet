# Create your views here.
from django.template import Context, loader
from django.http import HttpResponse
from bandwidth.models import NetworkDefault


import silk
import netflow
import heapq
from collections import OrderedDict


def _getInternalNetwork(request):
    user_name = "guest"
    if request.user.is_authenticated():
        user_name = request.user

    default_network = NetworkDefault.getDefaultIPSet(user_name)
    return default_network

def _toptalkers(ipobject, last_hour=2, topn=10):
    """
    Determine how much data is being sent.

    rec.sip is the sender of the data

    @param topn: How many of the top entries to return
    @return a list of the top talkers
    """

    flows = netflow.NetFlow()
    flows.setDateTime(last_hour)

    ip_data = {}

    for rec in flows.getRecords():
        if rec.sip in ipobject:
            ip = "%s"%(rec.sip)
            if ip_data.has_key(ip):
                ip_data[ip] += rec.bytes
            else:
                ip_data[ip] = rec.bytes

    top_talkers = [ (ip, "%0.2f MB"%(netflow.NetFlow.bytesToMB(
                    ip_data[ip]))) for ip in heapq.nlargest(
                    int(topn), ip_data, key = lambda k: ip_data[k])
                  ]

    return top_talkers

def _topreceivers(ipobject, last_hour=3, topn=10):
    """
    Determine how much data is being received and by who.

    rec.dip is the receiver of the data

    @param topn: How many of the top entries to return
    @return a list of the top talkers
    """

    flows = netflow.NetFlow()
    flows.setDateTime(last_hour)

    ip_data = {}

    for rec in flows.getRecords():
        if rec.dip in ipobject:
            ip = "%s"%(rec.dip)
            if ip_data.has_key(ip):
                ip_data[ip]+=rec.bytes
            else:
                ip_data[ip] = rec.bytes

    top_receivers = [ (ip, "%0.2f MB"%(netflow.NetFlow.bytesToMB(
                        ip_data[ip]))) for ip in heapq.nlargest(
                        int(topn), ip_data, key = lambda k: ip_data[k]) 
                    ]



    return top_receivers


def index(request, last_hour=2, interval=300):

    ipobject = _getInternalNetwork(request)

    flows = netflow.NetFlow()
    flows.setDateTime(last_hour)

    buckets = flows.getBuckets(interval)

    ip_data_in = OrderedDict()
    ip_data_out = OrderedDict()
    xAxisLabel = []

    for b in buckets:
        ip_data_in[b] = 0
        ip_data_out[b] = 0
        xAxisLabel.append(netflow.NetFlow.timestampToLabel(b * interval))

    for rec in flows.getRecords():
        rec_buckets, bucket_bytes = netflow.NetFlow.getBytesToBuckets(
            rec.stime_epoch_secs,
            rec.etime_epoch_secs,
            interval,
            rec.bytes)

        #Netflow data is retrieved by the sensor name, or the default
        #"all" which includes all sensors. We allow the user to see only
        #specific network segments or IP addresses. So we need to check
        #the sip(source ip) and dip(destination ip) for each flow.
        matching_buckets = set(buckets).intersection( set(rec_buckets))
        if rec.dip in ipobject:
            for matchb in matching_buckets:
                ip_data_in[matchb] += bucket_bytes

        if rec.sip in ipobject:
            for matchb in matching_buckets:
                ip_data_out[matchb] += bucket_bytes

    for b in buckets:
        #rint ip_data_in[b]
        ip_data_in[b] = netflow.NetFlow.bytesToMbps(ip_data_in[b], interval)
        ip_data_out[b] = netflow.NetFlow.bytesToMbps(ip_data_out[b], interval)

    tickinterval = len(buckets) / 8

    c = Context({
        'tickinterval' : tickinterval,
        'xAxisLabel' : xAxisLabel,
        'data_in' : [ (bucket, ip_data_in[bucket] ) for bucket in ip_data_in ],
        'data_out' : [ (bucket, ip_data_out[bucket] ) for bucket in ip_data_out ],
        'top_talkers' : _toptalkers(ipobject, last_hour),
        'top_receivers' : _topreceivers(ipobject, last_hour),
        })


    t = loader.get_template('bandwidth/dashboard.html')
    return HttpResponse(t.render(c))
