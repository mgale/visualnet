"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

import os, sys

#SOURCE_PATH = os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])), '..')
#SETTINGS_PATH = os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])), '..')
#sys.path.append(SOURCE_PATH)
#sys.path.append(SETTINGS_PATH)
#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

from django.conf import settings
from django.test import TestCase
from django.test.client import Client
from django.test.client import RequestFactory
import mock
import netflow
import silk


#Specify test file
netflow_test_file = os.path.join(settings.SITE_ROOT,"test_data/testflow_2012_07_31.dump")

def fake_default_IP(fake_request):
    return silk.IPWildcard(str("192.168.1.0/24"))

def fake_netflow_records(fake_sensor):
    flowfile = silk.SilkFile(netflow_test_file, silk.READ)
    netflow_test_data = list(flowfile)
    flowfile.close()
    return netflow_test_data

class TopRecievers(TestCase):
    def setUp(self):
        self.client = Client()

    @mock.patch("netflow.NetFlow.getRecords", fake_netflow_records)
    @mock.patch("bandwidth.views._getInternalNetwork", fake_default_IP)
    def test_top_n(self):

        response = self.client.get('/bandwidth/topreceivers/')

        self.assertEqual(response.status_code, 200)

    @mock.patch("netflow.NetFlow.getRecords", fake_netflow_records)
    @mock.patch("bandwidth.views._getInternalNetwork", fake_default_IP)
    def test_top_5(self):

        expected_data = [('192.168.1.67', '51.34 MB'), ('192.168.1.3', '23.00 MB'), ('192.168.1.40', '22.52 MB'), ('192.168.1.36', '6.76 MB'), ('192.168.1.35', '5.09 MB')]
        response = self.client.get('/bandwidth/topreceivers/5/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['top_talkers_list']), 5)
        self.assertListEqual(expected_data, response.context['top_talkers_list'])


class TopTalks(TestCase):
    def setUp(self):
        self.client = Client()
 
    @mock.patch("netflow.NetFlow.getRecords", fake_netflow_records)
    @mock.patch("bandwidth.views._getInternalNetwork", fake_default_IP)
    def test_top_n(self):

        response = self.client.get('/bandwidth/toptalkers/')

        self.assertEqual(response.status_code, 200)

    @mock.patch("netflow.NetFlow.getRecords", fake_netflow_records)
    @mock.patch("bandwidth.views._getInternalNetwork", fake_default_IP)
    def test_top_5(self):

        expected_data = [('192.168.1.35', '9.34 MB'), ('192.168.1.67', '3.74 MB'), ('192.168.1.1', '1.82 MB'), ('192.168.1.36', '1.24 MB'), ('192.168.1.40', '1.17 MB')]
        response = self.client.get('/bandwidth/toptalkers/5/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['top_talkers_list']), 5)
        self.assertListEqual(expected_data, response.context['top_talkers_list'])
