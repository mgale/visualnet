from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import redirect_to
from django.conf import settings
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'visualnet.views.home', name='home'),
    # url(r'^visualnet/', include('visualnet.foo.urls')),

    url(r'^$', redirect_to, {'url': '/bandwidth/'}),
    url(r'^bandwidth/$', 'bandwidth.views.index'),
    url(r'^bandwidth/(?P<last_hour>\d+)/$', 'bandwidth.views.index'),
    url(r'^bandwidth/(?P<last_hour>\d+)/(?P<interval>\d+)/$', 'bandwidth.views.index'),
    url(r'^bandwidth/toptalkers/$', 'bandwidth.views.toptalkers'),
    url(r'^bandwidth/toptalkers/(?P<topn>\d+)/$', 'bandwidth.views.toptalkers'),
    url(r'^bandwidth/topreceivers/$', 'bandwidth.views.topreceivers'),
    url(r'^bandwidth/topreceivers/(?P<topn>\d+)/$', 'bandwidth.views.topreceivers'),
    
    url(r'^detailedIP/(?P<ipaddr>.*)/topports/$', 'detailedIP.views.topports'),
    url(r'^detailedIP/(?P<ipaddr>.*)/toptalkers/$', 'detailedIP.views.toptalkers'),
    url(r'^detailedIP/(?P<ipaddr>.*)/topreceivers/$', 'detailedIP.views.toptalkers', {'dip' : False}),
    url(r'^detailedIP/(?P<ipaddr>.*)/$', 'detailedIP.views.index'),
    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT}),
)
