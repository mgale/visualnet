# Create your views here.
from django.template import Context, loader
from django.http import HttpResponse

import netflow
import heapq
from collections import OrderedDict


def index(request, ipaddr=None, last_hour=2, interval=300):

    ipobject = netflow.NetFlow.getIPWildCard(ipaddr)

    flows = netflow.NetFlow()
    flows.setDateTime(last_hour)

    buckets = flows.getBuckets(interval)

    ip_data_in = OrderedDict()
    ip_data_out = OrderedDict()
    ip_data_total = OrderedDict()
    xAxisLabel = []

    for b in buckets:
        ip_data_in[b] = 0
        ip_data_out[b] = 0
        ip_data_total[b] = 0
        xAxisLabel.append(netflow.NetFlow.timestampToLabel(b * interval))

    for rec in flows.getRecords():
        bucket = int(rec.etime_epoch_secs) / int(interval)

        if rec.dip in ipobject:                
            ip_data_in[bucket]+=rec.bytes
        else:
            ip_data_out[bucket]+=rec.bytes

        ip_data_total[bucket]+=rec.bytes

    for b in buckets:
        ip_data_in[b] = netflow.NetFlow.bytesToMbps(ip_data_in[b], interval)
        ip_data_out[b] = netflow.NetFlow.bytesToMbps(ip_data_out[b], interval)
        ip_data_total[b] = netflow.NetFlow.bytesToMbps(ip_data_total[b], interval)


    tickinterval = len(buckets) / 8

    c = Context({
        'ipaddr' : ipaddr,
        'tickinterval' : tickinterval,
        'xAxisLabel' : xAxisLabel,
        'data_in' : [ (bucket, ip_data_in[bucket] ) for bucket in ip_data_in ],
        'data_out' : [ (bucket, ip_data_out[bucket] ) for bucket in ip_data_out ],
        'data_total' : [ (bucket, ip_data_total[bucket] ) for bucket in ip_data_total ],
        })

    t = loader.get_template('detailedIP/graph.html')
    return HttpResponse(t.render(c))
    

def toptalkers(request, ipaddr, dip=True, topn=15):
    """
    Determine who is sending or receiving data to this ipaddr

    rec.sip is the sender of the data
    dip=True means that ipaddr is the receiver of data
    @return a list of the top talkers
    """

    flows = netflow.NetFlow()
    flows.setDateTime(2)
    
    ip_data = {}
    ipobject = netflow.NetFlow.getIPWildCard(ipaddr)

    for rec in flows.getRecords():
        if dip == True:
            my_ip = rec.dip
            other_host = rec.sip
            data_direction = "Top Data Producers"
        else:
            my_ip = rec.sip
            other_host = rec.dip
            data_direction = "Top Data Receivers"

        if my_ip in ipobject:
            mb = netflow.NetFlow.bytesToMB(rec.bytes)
            if ip_data.has_key(other_host):
                ip_data[other_host]+=mb
            else:
                ip_data[other_host] = mb

    c = Context({
        'title' : data_direction,
        'top_talkers_list' : [ (ip, "%0.2f MB"%(ip_data[ip])) for ip in heapq.nlargest(
                int(topn), ip_data, key = lambda k: ip_data[k]) ],
        })

    t = loader.get_template('detailedIP/list.html')
    return HttpResponse(t.render(c))

def topports(request, ipaddr, topn=10):
    """
    Determine who is sending or receiving data to this ipaddr by port

    rec.sip is the sender of the data
    @return a list of the top talkers
    """
    flows = netflow.NetFlow()
    flows.setDateTime(2)
    
    ip_data = {}
    ipobject = netflow.NetFlow.getIPWildCard(ipaddr)

    for rec in flows.getRecords():
        ip_match = False
        if rec.dip in ipobject:
            ip_match = True

        if rec.sip in ipobject:
            ip_match = True

        if ip_match:
            dport = rec.dport
            mb = netflow.NetFlow.bytesToMB(rec.bytes)
            if ip_data.has_key(dport):
                ip_data[dport]+=mb
            else:
                ip_data[dport] = mb

    c = Context({
        'title_pie' : "Test",
        'top_talkers_list' : [ [ "%s"%(port), ip_data[port] ] for port in heapq.nlargest(
                int(topn), ip_data, key = lambda k: ip_data[k]) ],
        })

    t = loader.get_template('detailedIP/pie.html')
    return HttpResponse(t.render(c))

