import datetime
import time
import silk
import decimal

from django.conf import settings

decimal.getcontext().prec = 2
time_format = "%Y/%m/%d:%H"
time_label_format = "%Y/%m/%d, %H:%M"


class NetFlow(object):

    def __init__(self):
        pass

    def setDateTime(self, start_time=2):

        start_time = -int(start_time)
        time_offset = datetime.timedelta(hours=start_time)

        end_time = datetime.datetime.now() + datetime.timedelta(hours=1)
        self.end_time = end_time.strftime(time_format)

        start_time = end_time + time_offset
        self.start_time = start_time.strftime(time_format)

        return True

    def getBuckets(self, size):

        start_epoch = time.mktime(time.strptime(self.start_time, time_format))
        end_epoch = time.mktime(time.strptime(self.end_time, time_format))

        buckets = []
        first_bucket = int(start_epoch) / int(size)
        end_bucket = int(end_epoch) / int(size)

        while first_bucket <= end_bucket:
            buckets.append(first_bucket)
            first_bucket += 1

        return buckets

    @classmethod
    def getBytesToBuckets(cls, start_epoch, end_epoch, size, bytes):

        buckets = []
        first_bucket = int(start_epoch) / int(size)
        end_bucket = int(end_epoch) / int(size)

        if start_epoch == 0:
            print "START IS ZERO"

        if end_epoch == 0:
            print "END IS ZERO"

        while first_bucket <= end_bucket:
            buckets.append(first_bucket)
            first_bucket += 1

        bucket_count = len(buckets)

        if bytes > 0 and bucket_count > 0:
            avg_byte = bytes / bucket_count
        else:
            avg_byte = 0

        return (buckets, avg_byte)


    @classmethod
    def getIPWildCard(cls, ipaddr):
        """
        We wrap the silk call so we do not have to import silk everywhere
        for this simple function.
        """
        return silk.IPWildcard(str(ipaddr))

    @classmethod
    def timestampToLabel(cls, timestamp):
        return datetime.datetime.fromtimestamp(timestamp).strftime(
            time_label_format)

    @classmethod
    def bytesToMB(cls, bytes):
        #1,048,576 when talking about memory
        #1,000,000 when talking about network transfers
        return float(bytes) / 1000000

    @classmethod
    def bytesToMbps(cls, bytes, seconds):
        # 1 byte is 8 bites
        return float(bytes) * 8 / 1000000 / seconds

    def getRecords(self, sensors=None):
        """
        Wrapping the silk.FGlob method.

        This method will get called for almost every request, this
        will allow for a single point of entry.
        """

        data_rootdir = None
        site_config_file = None

        if hasattr(settings, 'DATA_ROOTDIR'):
            data_rootdir = settings.DATA_ROOTDIR

        if hasattr(settings, 'SITE_CONFIG_FILE'):
            site_config_file = settings.SITE_CONFIG_FILE

        for recordfile in silk.FGlob(classname="all", type="all",
            sensors=sensors,
            start_date=self.start_time,
            end_date=self.end_time,
            data_rootdir=data_rootdir,
            site_config_file=site_config_file):

            for rec in silk.SilkFile(recordfile, silk.READ):
                yield rec
