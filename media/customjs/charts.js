// Defining visualnet namespace
if (typeof visualnet_JSNS == "undefined") { var visualnet_JSNS = {}; }

visualnet_JSNS.area_chart1 = function(xAxisLabel, tickinterval, data_in, data_out) {
        var chart1; // globally available
              chart1 = new Highcharts.Chart({
                 chart: {
                    renderTo: 'container',
                    type: 'area'
                 },

                 tooltip: {
                    formatter: function() {
                        return 'The value for <b>'+ this.x +
                        '</b> is <b>'+ this.y.toFixed(2) +'</b>';
                    }
                 },

                 title: {
                    text: 'Data Rates'
                 },
                 xAxis: {
                    categories: xAxisLabel,
                    tickInterval: tickinterval
                 },

                 yAxis: {
                    title: {
                       text: 'Mbps'
                    },
                 },

                 plotOptions: {
                    area: {
                        stacking: 'normal',
                        lineColor: '#666666',
                        lineWidth: 1,
                        marker: {
                            lineWidth: 1,
                            lineColor: '#666666'
                        }
                    },
                    series: {
                        enableMouseTracking: false,
                        shadow: false,
                        animation: false
                    }
                 },
                 series: [{
                    name: 'Data In',
                    data: data_in
                 }, {
                    name: 'Data Out',
                    data: data_out
                 }]

              });
}